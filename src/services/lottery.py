import datetime
import pytz
import calendar


weekdays_map = {
    calendar.SUNDAY: 0,
    calendar.MONDAY: 1,
    calendar.TUESDAY: 2,
    calendar.WEDNESDAY: 3,
    calendar.THURSDAY: 4,
    calendar.FRIDAY: 5,
    calendar.SATURDAY: 6,
}


class DrawDate(object):

    def __init__(self, weekday, time):
        self.weekday = weekday
        self.time = time

    def __gt__(self, start_date):

        self_wd = weekdays_map[self.weekday]
        obj_wd = weekdays_map[start_date.weekday()]

        return self_wd >= obj_wd and \
            datetime.datetime.combine(
                start_date.date(), self.time
            ) > start_date


class DrawDateCalculator(object):

    def __init__(self, timezone_name='Europe/Dublin', draw_dates=None):
        """ draw_dates must be in order """

        self.timezone = pytz.timezone(timezone_name)
        self.draw_dates = draw_dates or [
            DrawDate(
                calendar.WEDNESDAY,
                datetime.time(20, tzinfo=self.timezone)
            ),
            DrawDate(
                calendar.SATURDAY,
                datetime.time(20, tzinfo=self.timezone)
            ),
        ]

    def get_next_draw_date(self, start_date=None):
        start_date = start_date or self.__get_start_date()
        next_draw = self.__get_nex_draw_weekday(start_date)
        return self.__get_next_date(next_draw, start_date)

    def __get_start_date(self):
        return datetime.datetime.now(self.timezone)

    def __get_nex_draw_weekday(self, start_date):
        try:
            return filter(lambda x: x > start_date, self.draw_dates)[0]
        except IndexError:
            return filter(lambda x: x != start_date, self.draw_dates)[0]

    def __get_next_date(self, draw, start_date):
        draw_date = datetime.datetime.combine(start_date.date(), draw.time)
        while not (draw_date > start_date and draw_date.weekday() == draw.weekday):
            draw_date += datetime.timedelta(days=1)

        return draw_date
