from itertools import permutations
from collections import Counter
import re


def normalize_str(string):
    return re.sub(r"\s+", "", string, flags=re.UNICODE).lower()


def find_anagrams_1(word, word_list):
    """ first implementation. Don't scale. """
    return list(
        set(word_list) & _build_anagrams(word) - {word}
    )


def _build_anagrams(word):
    return {''.join(w) for w in permutations(word)}


def find_anagrams(word, word_list):
    return list(
        {w for w in word_list
         if Counter(normalize_str(w)) == Counter(normalize_str(word))}
        - {word}
    )
