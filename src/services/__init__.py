from .lottery import DrawDateCalculator
from .strings import find_anagrams
from .cache import lru_cache


def get_next_lottery_draw_date(*args, **kwargs):
    service = DrawDateCalculator()
    return service.get_next_draw_date(*args, **kwargs)
