from collections import OrderedDict
import logging
from functools import wraps


class LRUCache(object):
    logger = None

    def __init__(self, max_size=100, name=None, log=False, logger_class=None):
        self.name = name
        self.hits, self.miss = 0, 0
        self._storage = OrderedDict()
        self.max_size = max_size
        self.logger = logger_class or logging.getLogger(self.__logger_name())

    def __len__(self):
        return len(self._storage)

    def __contains__(self, key):
        return key in self._storage

    def store(self, key, value):
        if len(self._storage) >= self.max_size:
            self._storage.popitem(last=False)

        if key in self._storage:
            del self._storage[key]

        self._storage[key] = value

    def get(self, key):
        try:
            obj = self._storage[key]
        except KeyError as error:
            self.miss += 1
            self.logger.warning(
                'cache miss: {}"'.format(error.message)
            )
        else:
            self.hits += 1
            return obj

    def __logger_name(self):
        return '{}.{}.{}'.format(
            __name__,
            self.__class__.__name__,
            self.name or id(self)
        )


def lru_cache(max_size=None):

    def decorator(func):
        cache = LRUCache(max_size=max_size, name=func.__name__)

        @wraps(func)
        def wrapper(*args, **kwargs):
            key = tuple(args), tuple(kwargs.items())
            cached = cache.get(key)
            if cached:
                return cached
            result = func(*args, **kwargs)
            cache.store(key, result)
            return result

        return wrapper

    return decorator
