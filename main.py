#!/bin/env python
import time
from datetime import datetime
from src.services import lru_cache, find_anagrams, get_next_lottery_draw_date


@lru_cache(max_size=20)
def expasive_function():
    time.sleep(3)
    return "Hey guys!"


class App(object):

    def run(self):
        print('getting the next lottery draw date')
        print(get_next_lottery_draw_date())

        print('\n' * 3)


        print('calling expansive_function for the first time')
        print(expasive_function())
        print('\n' * 3)
        print('calling expansive_function for the second time')
        print(expasive_function())

        print('\n' * 3)

        print('finding anagrams for "Tom Marvolo Riddle"')
        print(find_anagrams(
            "Tom Marvolo Riddle", ["I am Lord Voldemort", "Hey bunny"]
        ))


if __name__ == '__main__':
    app = App()
    app.run()
