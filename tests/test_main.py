from unittest import TestCase
from main import App


class AppTestCase(TestCase):

    def setUp(self):
        self.app = App()

    def test_instantiate(self):
        self.assertIsInstance(self.app, App)

    def test_run(self):
        self.assertIsNone(self.app.run())
