from unittest import TestCase
from src.services import find_anagrams as tested_function


class AnagramFinderTestCase(TestCase):

    def test_find_anagrams(self):
        result = tested_function('word', ['word', 'word'[::-1]])
        self.assertIn('word'[::-1], result)

    def test_find_anagrams_does_not_return_original_word(self):
        result = tested_function('word', ['word', 'word'[::-1]])
        self.assertNotIn('word', result)

    def test_find_anagrams_voldemort(self):
        result = tested_function(
            'tommarvoloriddle',
            ['iamlordvoldemort', 'iamharrypotter']
        )
        self.assertIn('iamlordvoldemort', result)
        self.assertNotIn('iamharrypotter', result)
