import calendar
import datetime
from unittest import TestCase
import mock
from src.services import get_next_lottery_draw_date as tested_function
from src.services.lottery import DrawDateCalculator, DrawDate


class DrawDateCalculatorTestCase(TestCase):

    def setUp(self):
        self.service = DrawDateCalculator()

    def test_instantiate(self):
        self.assertIsInstance(self.service, DrawDateCalculator)

    @mock.patch('src.services.DrawDateCalculator')
    def test_function_wo_args(self, m_service):
        tested_function()
        m_service.assert_called_once_with()
        m_service().get_next_draw_date.assert_called_once_with()

    @mock.patch('src.services.DrawDateCalculator')
    def test_function_with_start_date(self, m_service):
        start_date = mock.Mock()
        tested_function(start_date)
        m_service.assert_called_once_with()
        m_service().get_next_draw_date.assert_called_once_with(start_date)

    @mock.patch.object(DrawDateCalculator,
                       '_DrawDateCalculator__get_next_date')
    @mock.patch.object(DrawDateCalculator,
                       '_DrawDateCalculator__get_start_date')
    def test_get_next_draw_date_wo_args_calls_get_start_date(self,
                                                             m_start_date, _):
        m_start_date.return_value = datetime.datetime.now(self.service.timezone)
        self.service.get_next_draw_date()
        m_start_date.assert_called_once_with()

    @mock.patch.object(DrawDateCalculator,
                       '_DrawDateCalculator__get_next_date')
    @mock.patch.object(DrawDateCalculator,
                       '_DrawDateCalculator__get_nex_draw_weekday')
    def test_get_next_draw_date_calls_next_draw_weekday(self, m_weekday, _):
        start_date = datetime.datetime.now(self.service.timezone)
        m_weekday.return_value = start_date
        self.service.get_next_draw_date(start_date)
        m_weekday.assert_called_once_with(start_date)

    def test_integration(self):
        start_day = datetime.datetime(2018, 11, 11, tzinfo=self.service.timezone)
        self.assertEqual(
            tested_function(start_day),
            datetime.datetime(2018, 11, 14, 20, tzinfo=self.service.timezone)
        )

    def test_integration_2(self):
        start_day = datetime.datetime(2018, 11, 14, 19, tzinfo=self.service.timezone)
        self.assertEqual(
            tested_function(start_day),
            datetime.datetime(2018, 11, 14, 20, tzinfo=self.service.timezone)
        )

    def test_integration_3(self):
        start_day = datetime.datetime(2018, 11, 14, 20, tzinfo=self.service.timezone)
        self.assertEqual(
            tested_function(start_day),
            datetime.datetime(2018, 11, 21, 20, tzinfo=self.service.timezone)
        )

    def test_integration_4(self):
        start_day = datetime.datetime(2018, 11, 10, 19, tzinfo=self.service.timezone)
        self.assertEqual(
            tested_function(start_day),
            datetime.datetime(2018, 11, 10, 20, tzinfo=self.service.timezone)
        )


class DrawDateTestCase(TestCase):

    def test_instantiate(self):
        self.assertIsInstance(DrawDate(0, 0), DrawDate)

    def test_wed_gt_sunday(self):
        dt1 = DrawDate(calendar.WEDNESDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 11)  # SUN
        self.assertTrue(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.SUNDAY)

    def test_sat_gt_sunday(self):
        dt1 = DrawDate(calendar.SATURDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 11)  # SUN
        self.assertTrue(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.SUNDAY)

    def test_tue_not_gt_wed(self):
        dt1 = DrawDate(calendar.TUESDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 7)  # WED
        self.assertFalse(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.WEDNESDAY)

    def test_thu_gt_wed(self):
        dt1 = DrawDate(calendar.THURSDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 7)  # WED
        self.assertTrue(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.WEDNESDAY)

    def test_sun_not_gt_sat(self):
        dt1 = DrawDate(calendar.SUNDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 10)  # SAT
        self.assertFalse(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.SATURDAY)

    def test_mon_gt_sat(self):
        dt1 = DrawDate(calendar.MONDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 10)  # SAT
        self.assertFalse(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.SATURDAY)

    def test_gt_wednesday_19_o_clock(self):
        dt1 = DrawDate(calendar.WEDNESDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 7, 19)  # WED
        self.assertTrue(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.WEDNESDAY)

    def test_gt_wednesday_20_o_clock(self):
        dt1 = DrawDate(calendar.WEDNESDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 7, 20)  # WED
        self.assertFalse(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.WEDNESDAY)

    def test_gt_saturday_19_o_clock(self):
        dt1 = DrawDate(calendar.SATURDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 10, 19)  # SAT
        self.assertTrue(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.SATURDAY)

    def test_gt_saturday_20_o_clock(self):
        dt1 = DrawDate(calendar.SATURDAY, datetime.time(20))
        dt2 = datetime.datetime(2018, 11, 10, 20)  # SAT
        self.assertFalse(dt1 > dt2)
        self.assertEqual(dt2.weekday(), calendar.SATURDAY)
