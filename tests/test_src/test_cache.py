import mock
from unittest import TestCase
from src.services.cache import LRUCache
from src.services import lru_cache


class LRUCacheTestCase(TestCase):

    def setUp(self):
        self.cache = LRUCache(max_size=5, name="mycache")
        self.expansive_service = mock.Mock()

    def test_add_to_cache(self):
        self.assertEqual(len(self.cache), 0)
        self.cache.store('key', 'value')
        self.assertEqual(len(self.cache), 1)

    def test_pop_old_items_when_max_value_reached(self):
        self.assertEqual(len(self.cache), 0)
        for k, v in zip('abcdef', [1, 2, 3, 4, 5, 6]):
            self.cache.store(k, v)
        self.assertEqual(len(self.cache), 5)
        self.assertIn('f', self.cache)
        self.assertNotIn('a', self.cache)

    def test_move_item_to_tail_when_accessed_again(self):
        self.cache.store('A', 1)
        self.cache.store('B', 2)
        self.cache.store('C', 3)
        self.cache.store('A', 4)

        self.assertEqual(self.cache._storage.items()[-1], ('A', 4))

    def test_get_item_from_cache(self):
        self.cache.store('A', 1)
        self.assertEqual(self.cache.hits, 0)
        self.assertEqual(self.cache.get('A'), 1)
        self.assertEqual(self.cache.get('A'), 1)
        self.assertEqual(self.cache.hits, 2)

    def test_get_miss_count(self):
        self.assertEqual(self.cache.miss, 0)
        self.cache.get('A')
        self.cache.get('BET')
        self.assertEqual(self.cache.miss, 2)

    @mock.patch('src.services.cache.logging')
    def test_as_decorator(self, m_logging):

        @lru_cache(max_size=2)
        def decorated_function(*args, **kwargs):
            self.expansive_service()
            return "these were my args: {}, and kwargs: {}".format(
                args, kwargs
            )

        result = decorated_function('test', company='betbrigth')
        self.assertEqual(
            result,
            "these were my args: ('test',), and kwargs: "
            "{'company': 'betbrigth'}"
        )

        m_logging.getLogger().warning.assert_called_once_with(
            'cache miss: ((\'test\',), ((\'company\', \'betbrigth\'),))"'
        )

        decorated_function('test', company='betbrigth')

        self.assertEqual(m_logging.getLogger().warning.call_count, 1)
        self.assertEqual(self.expansive_service.call_count, 1)
